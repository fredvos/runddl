# RUNDDL

## Purpose

Runs DDL CREATE or DROP statements for a SQL database.

## Properties

Tables and their dependencies, indexes and constraints are described in a Java properties file.

Here's an example:
```
ddl.tables = organisations departments

ddl.table.organisations.dependencies =
ddl.table.organisations.create.101 = \
  CREATE TABLE organisations ( \
    id UUID NOT NULL, \
    name VARCHAR(32) NOT NULL, \
    description VARCHAR(100) \
  )
ddl.table.organisations.create.201 = ALTER TABLE organisations ADD PRIMARY KEY (id)
ddl.table.organisations.create.202 = ALTER TABLE organisations ADD CONSTRAINT org_uk_name UNIQUE (name)
ddl.table.organisations.create.301 = \
  CREATE TRIGGER org_auto_id \
  BEFORE INSERT ON organisations \
  REFERENCING NEW ROW AS NEW \
  FOR EACH ROW SET NEW.id=UUID()
ddl.table.organisations.drop = DROP TABLE organisations

ddl.table.departments.dependencies = organisations
ddl.table.departments.create.101 = \
  CREATE TABLE departments ( \
    id UUID NOT NULL, \
    org_id UUID NOT NULL, \
    name VARCHAR(32) NOT NULL, \
    description VARCHAR(100) \
  )
ddl.table.departments.create.201 = ALTER TABLE departments ADD PRIMARY KEY (id)
ddl.table.departments.create.202 = ALTER TABLE departments ADD CONSTRAINT dep_uk_name UNIQUE (name)
ddl.table.departments.create.203 = ALTER TABLE departments ADD FOREIGN KEY (org_id) REFERENCES organisations (id)
ddl.table.departments.create.301 = \
  CREATE TRIGGER dep_auto_id \
  BEFORE INSERT ON departments \
  REFERENCING NEW ROW AS NEW \
  FOR EACH ROW SET NEW.id=UUID()
ddl.table.departments.drop = DROP TABLE departments
```

Property ``ddl.tables`` lists the tables in scope, separated by spaces.

Property ``ddl.table.<TABLE>.dependencies`` lists tables where this table depends on.
This determines the order in which CREATE and DROP statements are fired.

Property ``ddl.table.<TABLE>.create.<ORDER>`` describes a single CREATE statement.
For a single table these are fired in ASCENDING order. 

Property ``ddl.table.<TABLE>.drop.<ORDER>`` describes a single DROP statement.
For a single table these are fired in ASCENDING order.

I use:

- 101 for TABLE
- 201-299 for CONSTRAINTS
- 301-399 for TRIGGERS
- 404-499 for COMMENTS 

## Usage

``<command> [-v] {create | drop} [options]``

Where ``<command>`` is ``java -jar runddl-<version>-jar-with-dependencies.jar`` or a script.

## Options

``=`` is not required for options.

### ``-v``

Logging at debug level with more details, for instance timestamps in milliseconds precision.
Default is at INFO level.

### ``--driver`` (required)

Full class name of driver (must be in classpath).
For HyperSQL the driver is included in runddl.
For HyperSQL use ``--driver=org.hsqldb.jdbc.JDBCDriver

### ``--connection-url`` (required)

Reference database.
The URL depends on the database driver and the features of the database.
For a HyperSQL file database in the ``/tmp`` directory, one can use ``connection-url="jdbc:hsqldb:file:/tmp/testdb"

### ``--username`` (required)

For a HyperSQL database user SA is often used.
In that case use ``--ussrname=SA``

### ``--password`` (optional)

### ``--properties-file`` (required)

File containing all CREATE and DROP statements.

## Example run using an HyperSQL memmory database

The database is removed when the JVM stops, so immediately after the command.

Saved sample text above as file ``/tmp/test.properties``.


```
$ java -jar target.v0.1/runddl-0.1-SNAPSHOT-jar-with-dependencies.jar create \
--driver=org.hsqldb.jdbc.JDBCDriver \
--connection-url="jdbc:hsqldb:mem:test" \
--username=SA \
--properties-file=/tmp/test.properties

INFO  - CREATE TABLE organisations ( id UUID NOT NULL, name VARCHAR(32) NOT NULL, description VARCHAR(100) )
INFO  - ALTER TABLE organisations ADD PRIMARY KEY (id)
INFO  - ALTER TABLE organisations ADD CONSTRAINT org_uk_name UNIQUE (name)
INFO  - CREATE TRIGGER org_auto_id BEFORE INSERT ON organisations REFERENCING NEW ROW AS NEW FOR EACH ROW SET NEW.id=UUID()
INFO  - CREATE TABLE departments ( id UUID NOT NULL, org_id UUID NOT NULL, name VARCHAR(32) NOT NULL, description VARCHAR(100) )
INFO  - ALTER TABLE departments ADD PRIMARY KEY (id)
INFO  - ALTER TABLE departments ADD CONSTRAINT dep_uk_name UNIQUE (name)
INFO  - ALTER TABLE departments ADD FOREIGN KEY (org_id) REFERENCES organisations (id)
INFO  - CREATE TRIGGER dep_auto_id BEFORE INSERT ON departments REFERENCING NEW ROW AS NEW FOR EACH ROW SET NEW.id=UUID()
```
