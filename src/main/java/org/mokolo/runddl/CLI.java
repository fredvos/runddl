/*
 * Copyright Ⓒ 2019 Fred Vos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mokolo.runddl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

import org.mokolo.commons.cli.CommandDefinition;
import org.mokolo.commons.cli.CommandParser;
import org.mokolo.commons.io.log4j.Log4JConfiguration;
import org.mokolo.commons.lang.document.Document;
import org.mokolo.commons.sql.DDLProcessor;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import lombok.Cleanup;
import lombok.extern.log4j.Log4j;

@Log4j
public class CLI {
  
  public static void main(String[] args) {

    try {
      CommandParser commandParser = new CommandParser(new OptionParser("v"), "request");
      commandParser.addCommandDefinition(new CommandDefinition("create",  null)
          .addRequiredArgument("driver")
          .addRequiredArgument("connection-url")
          .addRequiredArgument("username")
          .addOptionalArgument("password")
          .addRequiredArgument("properties-file"));
      commandParser.addCommandDefinition(new CommandDefinition("drop",  null)
          .addRequiredArgument("driver")
          .addRequiredArgument("connection-url")
          .addRequiredArgument("username")
          .addOptionalArgument("password")
          .addRequiredArgument("properties-file"));
      OptionSet options = commandParser.getOptionParser().parse(args);

      Log4JConfiguration log4j = new Log4JConfiguration();
      Log4JConfiguration.Level level = Log4JConfiguration.Level.NORMAL;
      if (options.has("v")) {
        level = Log4JConfiguration.Level.VERBOSE;
        log4j.addConsoleAppender(level, Log4JConfiguration.WITH_DATETIMESTAMP);
      }
      else
        log4j.addConsoleAppender(level, Log4JConfiguration.NONE);
      log4j.addLogger("org.mokolo", level);

      Document document = commandParser.parseCommandWithSwitches(options);
      commandParser.validateDocument(document);
      
      if (document.getValue("request").equals("create") || document.getValue("request").equals("drop")) {
        String driver = document.getValue("driver");
        String connectionUrl = document.getValue("connection-url");
        String username = document.getValue("username");
        String password = document.getValue("password");
        if (password == null)
          password = "";
        File propertiesFile = document.getValueAsFile("properties-file");

        Properties ddlProperties = new Properties();
        @Cleanup InputStream is = new FileInputStream(propertiesFile);
        ddlProperties.load(is);
        DDLProcessor processor = new DDLProcessor(ddlProperties);
        
        Class.forName(driver);
        Connection connection = DriverManager.getConnection(connectionUrl, username, password);
        Statement statement = connection.createStatement();
        
        if (document.getValue("request").equals("create")) {
          for (String ddlCreateStatement : processor.listCreateAll()) {
            log.info(ddlCreateStatement);
            statement.executeUpdate(ddlCreateStatement);
          }
        }
        else {
          for (String ddlCreateStatement : processor.listDropAll()) {
            log.info(ddlCreateStatement);
            statement.executeUpdate(ddlCreateStatement);
          }
        }
        connection.commit();
      }
    } catch (Exception e) {
      System.err.println(e.getClass().getName()+" occurred. Msg="+e.getMessage());
      e.printStackTrace();
    }
  }

}
